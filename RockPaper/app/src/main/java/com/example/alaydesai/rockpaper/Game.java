package com.example.alaydesai.rockpaper;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.seismic.ShakeDetector;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.squareup.seismic.ShakeDetector;

public class Game extends AppCompatActivity implements ShakeDetector.Listener  {

    private static final String TAG = "Alay";

    boolean lightOn = false;
    TextView DisplayText;

    int shakesCount = 0;
    int[] photos = {R.drawable.scissors,R.drawable.paper,R.drawable.rock,R.drawable.lizard,R.drawable.spock};
    String option= "";

    FirebaseFirestore db;

    Random ran = new Random();
    int i = ran.nextInt(photos.length);

    EditText user ;
    String name;

    Button btnPlay;
    EditText txtName;
    ImageView img;
    TextView winner;


   // public static final String TAG = "JENELLE";


    final String[] names ={"Rock","Paper","Scissors","Lizard","Spock"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        winner = (TextView)findViewById(R.id.winner);
        // 1. create a bunch of nonsense variables
        // that are used to detect when the phone is shaking
        // (setup your phone to deal with shakes)
        SensorManager manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        ShakeDetector detector = new ShakeDetector(this);
        detector.start(manager);




    }

    // this a mandatory function
    // it's required by the ShakeDetector.Listener class
    @Override public void hearShake() {

        shakesCount = shakesCount + 1;
        if (shakesCount == 3) {
            Log.d("JENELLE", "phone is shaking!!!!");

            Toast.makeText(this, "PHONE IS SHAKING!!!!" + shakesCount, Toast.LENGTH_SHORT).show();
            ImageView image = (ImageView) findViewById(R.id.img);

            i = ran.nextInt(photos.length);
            image.setImageResource(photos[i]);
            shakesCount = 0;

            //setting the option variable accroding to the photo display in screen
            if (photos[i] == R.drawable.scissors) {
                option = "scissor";
                winner.setText(names[2]);


            }
            if (photos[i] == R.drawable.paper) {
                option = "paper";
                winner.setText(names[1]);

            }
            if (photos[i] == R.drawable.rock) {
                option = "rock";
                winner.setText(names[0]);

            }
            if (photos[i] == R.drawable.lizard) {
                option = "lizard";
                winner.setText(names[3]);

            }
            if (photos[i] == R.drawable.spock) {
                option = "spock";
                winner.setText(names[4]);

            }
        }
    }








}