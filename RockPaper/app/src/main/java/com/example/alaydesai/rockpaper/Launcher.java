package com.example.alaydesai.rockpaper;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

public class Launcher extends AppCompatActivity {

    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        final Context context = this;

        Thread timer = new Thread(){
            public void run(){

                try{
                    imageView=(ImageView)findViewById(R.id.imageView);

                    Thread.sleep(3000);

                }catch(Exception ex){
                    //android.widget.Toast.makeText(context, "Something went wrong",android.widget.Toast.LENGTH_LONG).show();
                }finally {
                    finish();
                   // startLogin();
                    startActivity(new Intent(context,Login.class));

                }

            }
        };
        timer.start();


    }

}
