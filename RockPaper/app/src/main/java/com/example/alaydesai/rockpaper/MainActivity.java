package com.example.alaydesai.rockpaper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnFindPlayer;
    Button btnCheckScore;
    Button btnInviteFriends;
    Button btnPlay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnFindPlayer = findViewById(R.id.btnFindPlayer);
        btnFindPlayer.setOnClickListener(this);

        btnCheckScore = findViewById(R.id.btnCheckScore);
        btnCheckScore.setOnClickListener(this);

        btnInviteFriends = findViewById(R.id.btnInviteFriends);
        btnInviteFriends.setOnClickListener(this);

        btnPlay = findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {

        if (v.getId() == btnFindPlayer.getId()) {

            Intent In = new Intent(this, MapsActivity.class);
            startActivity(In);

        }
        else if (v.getId() == btnPlay.getId()) {

            Intent In = new Intent(this, Game.class);
            startActivity(In);

        }
        else if (v.getId() == btnCheckScore.getId()) {

            Intent In = new Intent(this, CheckScore.class);
            startActivity(In);

        }
        else if(v.getId() == btnInviteFriends.getId()) {

            Intent In = new Intent(this, InviteFriends.class);
            startActivity(In);

        }





    }
}
